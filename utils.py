import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt



def grayscale(image):
    gray = cv.cvtColor(image.copy(), cv.COLOR_BGR2GRAY)
    ret, binary = cv.threshold(gray,254,255,cv.THRESH_BINARY_INV)
    return binary

def removeBlackDots(image):
    gray = cv.cvtColor(image.copy(), cv.COLOR_BGR2GRAY)
    ret, binary = cv.threshold(gray,254,255,cv.THRESH_BINARY_INV)
    element = cv.getStructuringElement(cv.MORPH_OPEN,(4,4))
    eroded = cv.erode(binary,element)
    dilated = cv.dilate(eroded,element)

    x, y = dilated.shape

    for i in range(x):
        for j in range(y):
            if (dilated[i][j] != 0):
                continue
            else:
                image[i][j]=255


    return image



#    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
#
#    x, y = gray.shape
#    for i in range(x):
#        for j in range(y):
#            if (gray[i][j] < 10):
#                image[i][j]=255
#
#    return image


def removeSquares(image, mask):
    x, y = mask.shape
    #print(image)
    for i in range(x):
        for j in range(y):
            if (mask[i][j]> 0 ):
                if (image[i][j][0] == 164 and image[i][j][1]==73 and image[i][j][2]==163):
                    mask[i][j]=0
    return mask

def getRedShape(image, mask):
    x, y = mask.shape

    for i in range(x):
        for j in range(y):
            if (mask[i][j]> 0 ):
                if (not(image[i][j][0] == 36 and image[i][j][1]==28 and image[i][j][2]==237)):
                    mask[i][j]=0
                    if(image[i][j][0] != 255 and  image[i][j][1] != 255 and image[i][j][2] != 255):
                        print(image[i][j])
    return mask



def InRangeByHelano(image, mask, lower, upper):
    x, y = mask.shape
    #print(image)
    for i in range(x):
        for j in range(y):
            if (mask[i][j]> 0 ):
                if (not((image[i][j][0]>= lower[0] and image[i][j][0] <= upper[0])  and (image[i][j][1]>= lower[1] and image[i][j][1] <= upper[1])  and (image[i][j][2]>= lower[2] and image[i][j][2] <= upper[2]) )):
                    mask[i][j]=0
    return mask

def getSkeleton(mask, source):
    element = cv.getStructuringElement(cv.MORPH_OPEN,(3,3))
    eroded = cv.erode(mask,element)
    done = False
    skel = np.zeros(mask.shape,np.uint8)
    size = np.size(mask)
    while( not done):
        eroded = cv.erode(mask,element)
        temp = cv.dilate(eroded,element)
        temp = cv.subtract(mask,temp)
        skel = cv.bitwise_or(skel,temp)
        mask = eroded.copy()
        zeros = size - cv.countNonZero(mask)
        if zeros==size:
            done = True
    return skel



def fillHoles(image):

    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    ret, binary = cv.threshold(gray,254,255,cv.THRESH_BINARY_INV)
    h, w = binary.shape[:2]
    imgFilled = binary.copy()
    mask = np.zeros((h+2, w+2), np.uint8)
    cv.floodFill(imgFilled, mask, (0,0), 255);
    result = cv.bitwise_not(imgFilled)
    teste = binary | result

    return teste


def fillHolesBinaryImage(image):


    h, w = image.shape[:2]
    imgFilled = image.copy()
    mask = np.zeros((h+2, w+2), np.uint8)
    cv.floodFill(imgFilled, mask, (0,0), 255);
    result = cv.bitwise_not(imgFilled)
    teste = image | result

    return teste


#needs to receive a image that was already submitted to the fillHoles function
def getConvexHull(image, source):


    image = removeSquares(source, image)
    gray = image.copy()

    contours, hierarchy = cv.findContours(gray, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    hull = []

    for i in range(len(contours)):
        hull.append(cv.convexHull(contours[i], False))

    drawing = np.zeros((gray.shape[0], gray.shape[1], 3), np.uint8)

    # draw contours and hull points
    for i in range(len(contours)):
        color = (255, 0, 0)
        cv.drawContours(image, hull, i, color, 1, 8)
        cv.drawContours(source, hull, i, color, 1, 8)

    return image, source


def hitOrMiss(image):
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    ret, binary = cv.threshold(gray.copy(),254,255,cv.THRESH_BINARY_INV)
    kernel = np.array((
        [1],
        [1],
        [1],
        [1],
        [1],
        [1],
        [1]), dtype="int")

    kernel2 = np.array((
        [1, 1, 1, 1, 1, 1, 1,1,1,1,1,1]), dtype="int")

    kernel3 = np.array((
        [0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0]), dtype="int")

    kernel4 = np.array((
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 1, 1, 0, 0],
        [0, 0, 1, 1, 1, 0, 0],
        [0, 0, 1, 1, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0]), dtype="int")




    output_image = cv.morphologyEx(binary, cv.MORPH_HITMISS, kernel)
    output_image3 = cv.morphologyEx(output_image, cv.MORPH_HITMISS, kernel3)
    output_image3 = cv.morphologyEx(output_image3, cv.MORPH_HITMISS, kernel4)
    output_image3 = cv.morphologyEx(output_image3, cv.MORPH_HITMISS, kernel4)
    output_image3 = cv.morphologyEx(output_image3, cv.MORPH_HITMISS, kernel4)
    output_image3 = cv.morphologyEx(output_image3, cv.MORPH_HITMISS, kernel4)
    output_image3 = cv.morphologyEx(output_image3, cv.MORPH_HITMISS, kernel4)
    output_image3 = cv.morphologyEx(output_image3, cv.MORPH_HITMISS, kernel4)

    return output_image3

def showResult(input, output):
    plt.subplot(121),plt.imshow(input, cmap = 'gray')
    plt.title('Input Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(output, cmap = 'gray')
    plt.title('output Image'), plt.xticks([]), plt.yticks([])
    plt.show()
