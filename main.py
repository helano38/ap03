import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
import utils

source = cv.imread('morfologia.png')

teste = source.copy()

#remoção dos pontos preto (1)
result = utils.removeBlackDots(teste.copy())

#preenchimento de buracos (2)
result2 = utils.fillHoles(teste.copy())
#fecho convexo dos objetos de cor vermelha, azul, verde(3)
mask,result3 = utils.getConvexHull(result2.copy(), source.copy())

lower = [36 , 28, 237]
upper = [70,50,240]

#print(source)
result5 = utils.InRangeByHelano(source.copy(), result2.copy(), lower, upper)


#esqueleto da imagem de cor vermelha (5)
result6 = utils.getSkeleton(result5.copy(), source.copy())
result4 = utils.hitOrMiss(result.copy())
mask2, result7 = utils.getConvexHull(result5.copy(), source.copy())

result8 = utils.fillHolesBinaryImage(mask2.copy())




#esqueleto forma vermelha depois do convex Hull (6)
result9 = utils.getSkeleton(result8.copy(), source.copy())
#print(result2)


result10 = utils.erode(source.copy())

gray = utils.grayscale(source.copy())

cv.imshow("output", gray)
cv.waitKey()
#utils.showResult(source, result4)
